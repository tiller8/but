# Exercice 23

## a

on peut approcher l’inverse de ce nombre avec une somme géométrique de la sorte

$\displaystyle\sum_{k=0}^nx^k+\frac{x^{n+1}}{1-x}$

## b

Il faut prendre -0.1 pour valeur de a

$b=\frac1{0.9}$
