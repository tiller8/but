| N°  | Libellé                         | Exemple                                  | P/C | Calcul              |
|:---:|:-------------------------------:|:----------------------------------------:|:---:|:-------------------:|
| 1   | ~~nom administration~~          | ~~Administration XYZ~~                   | P   | Paramètre           |
| 2   | ~~adresse administration~~      | ~~123 rue du chemin<br/>31000 Toulouse~~ | P   | Paramètre           |
| 3   | n° engagement                   | 12345                                    | P   |                     |
| 4   | n° commande                     | 0905                                     | P   |                     |
| 5   | date commande                   | 15/04/2019                               | P   |                     |
| 6   | société fournisseur             | société Durand                           | P   |                     |
| 7   | nom voie société fournisseur    | rue du Pont                              | P   |                     |
| 8   | numéro rue société fournisseur  | 34                                       | P   |                     |
| 9   | code postal société fournisseur | 31700                                    | P   |                     |
| 10  | ville société fournisseur       | Blagnac                                  | P   |                     |
| 11  | référence produit               | C01                                      | P   |                     |
| 12  | désignation produit             | Crayon                                   | P   |                     |
| 13  | quantité produit                | 10                                       | P   |                     |
| 14  | prix unitaire HT produit        | 1€                                       | P   |                     |
| 15  | ~~montant HT produit~~          | ~~14€~~                                  | C   | [13]×[14]           |
| 16  | ~~taux TVA~~                    | ~~20%~~                                  | P   | Paramètre           |
| 17  | ~~prix unitaire TTC produit~~   | ~~42€~~                                  | C   | [14]+[14]×[16]      |
| 18  | ~~montant TTC produit~~         | ~~66€~~                                  | C   | [17]×[13]           |
| 19  | ~~Total HT~~                    | ~~134€~~                                 | C   | [15]1+[15]2+…+[15]n |
| 20  | ~~Total TTC~~                   | ~~777€~~                                 | C   | [15]+[15]×[16]      |
| 21  | date facture                    | ~~30/04/2019~~                           | P   |                     |
| 22  | n° facture                      | ~~FA19040012~~                           | P   |                     |
| 23  | date lettre                     | 02/05/2014                               | P   |                     |
| 24  | objet lettre                    | Refus marchandise                        | P   |                     |
| 25  | n° mandat                       | 4148                                     | P   |                     |
| 26  | date mandat                     | 06/05/2019                               | P   |                     |
| 27  | IBAN fournisseur                | FR76 12357543132462234                   | P   |                     |
| 28  | a payer mandat                  | 1234€                                    | P   |                     |


