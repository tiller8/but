| MCDifié | N°  | Rubrique                     | Exemple                       | C / P | Formule    |
|:-------:|:---:|:----------------------------:|:-----------------------------:|:-----:|:----------:|
| ×       | 1   | Nom société                  | Botoutou                      | P     | Paramètre  |
| O       | 2   | Nom chien                    | Calin                         | P     |            |
| ×       | 3   | Âge chien (au 1er javier)    | 8 ans 2 mois                  | C     | [5] − [4]  |
| O       | 4   | Date naissance chien         | 05/06/2007                    | P     |            |
| ×       | 5   | Date 1er janvier             | 01/01/2021                    | P     | Paramètre  |
| O       | 6   | Sexe chien                   | M                             | P     |            |
| O       | 7   | Race chien                   | Labrador                      | P     |            |
| O       | 8   | Intitulé race                | Berger                        | P     |            |
| O       | 9   | Origine race                 | Polynésie                     | P     |            |
| O       | 10  | Descriptif race              | Intelligent, facile à dresser | P     |            |
|         | 11  | Nom propriétaire             | Jean                          | P     |            |
|         | 12  | Adresse propriétaire         | Ici                           | P     |            |
|         | 13  | Chien(s) propriétaire        | Toutou<br/>Wouwou             | P     |            |
|         | 14  | Acquisition du chien         | 08/11/2012                    | P     |            |
|         | 15  | Ville concours               | Castres                       | P     |            |
|         | 16  | Date concours                | 12/12/2012                    | P     |            |
|         | 17  | Chiens primés concours       | 7                             | P     |            |
|         | 18  | Chiens participants concours | 42                            | P     |            |
|         | 19  | Classement chien concours    | 0^e^                          | P     |            |
|         | 20  | Âge chien concours           | 1 an                          | C     | [16] − [4] |
