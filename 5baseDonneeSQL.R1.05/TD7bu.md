| N°  | Libellé               | Exemple                   | P/C | Calcul |
| --- | --------------------- | ------------------------- | --- | ------ |
| 1   | Nom étudiant          | Dupond                    | P   |        |
| 2   | Prénom étudiant       | Jean                      | P   |        |
| 3   | Adresse étudiant      | 1 rue première            | P   |        |
| 4   | Numéro étudiant       | 5480546532                | P   |        |
| 5   | Code livre            | 4521956462                | P   |        |
| 6   | Titre livre           | dictionnaire encyclopédie | P   |        |
| 7   | Nom auteur(s)         | Dupont                    |     |        |
| 8   | Prénom auteur(s)      | Jean                      |     |        |
| 9   | Nationalité auteur(s) | Polonaise                 |     |        |
| 10  | Date prêt             | 01/02/2003                |     |        |
| 11  | Date retour prêt      | 02/03/2004                |     |        |
