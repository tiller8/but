Guilhem Faure
# TP5 interfaces web
## manga news
aucun changement d’apparence
## lequipe
change aux largeurs :
- 928 px
- 624 px
le style est mélangé au code html.
## discord
change aux largeurs :
- 1024 px
- 768 px
- 648 px
## raspberry
- 1024 px
- 700 px
- 500 px
## question
Parmi ces sites, seul le site de mange news n’est pas adaptatif aux différentes tailles d’écran.
Généralement, les lignes contenant @media permettent de changer de style.
Le menu de navigation est structuré avec la baline "<nav>".
