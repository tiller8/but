# Gestion de projet — Leçon 1

Le client peut s’investir dans le projet en participant à certaines réunions, prêtant du matériel ou de la main d’œuvre. Dans ce cas, bien le préciser dans le cahier des charges.

> Si difficulté à trouver les fonctions contraintes, tenir compte de l’environnement du produit.

## Outils pour trouver des fonctions secondaires

- Analyse des risques

- Analyse de l’environnement
