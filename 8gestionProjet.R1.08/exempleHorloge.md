| Fonctions | Critères                                                                                        | Niveaux                                                    | Flexibilité            |
|:---------:| ----------------------------------------------------------------------------------------------- | ---------------------------------------------------------- |:----------------------:|
| FP        | Lisibilité<br/>Exactitude<br/>Précision                                                         | Au moins à 5 m<br/>10 s de décalage par an<br/>À la minute | ± 15 s                 |
| FC~1~     | Couleur du cadran<br/>Numerique<br/>LED blanches, Dimensions                                    | Rouge<br/>OUI<br/>OUI<br/>40×20×10<br/>rectangulaire       |                        |
| FC~2~     | Poids<br/>outils nécessaires maintenance<br/>Notice en français<br/>temps de pose nécessaire à2 | Max 1kg<br/>OUI<br/>OUI<br/>10 min maxi.                   |                        |
| FC~3~     | Humidité<br/>Pannes<br/>Temp. Maxi.<br/>Temp. Mini.                                             | 80%<br/>0 les 2 prem années<br/>60°<br/>−20°               | <br/><br/><br/>±<br/>± |
| FC~4~     | Nombre de piles<br/>Type pile<br/>Fournies                                                      | 2 par an<br/>AA<br/>OUI                                    |                        |
| FC~5~     | Px de vente HT<br/>Délai paiment                                                                | 20€ HT l’unité<br/>Au comptant                             |                        |
