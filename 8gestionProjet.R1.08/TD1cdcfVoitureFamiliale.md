# TD1 — Cahier des charges fonctionnel : Voiture Familiale

## Consigne

Vous êtes employé dans le bureau d’études dans la société WNB de voitures. La direction vous demande de réaliser le CDCF d’une voiture familiale. En effet, suite à de nombreuses études en France, il s’avère qu’il existe un réel besoin.
Cette voiture doit s’adresser aux familles ayant au moins deux enfants, avec un revenu inférieur à 30 000 € / an.
Dans un premier temps, la direction joue le rôle de client. Le bureau d’études joue donc le rôle de maître d’œuvre. 

## Partie I

### Présentation de l’agence

### Présentation du client : société WMB

### Présentation du projet

#### moyen pédagogique : bête à cornes

Utilisateur : la famille                           Agit sur : position de la famille sur la route

                                    Voiture familiale

                                But : aider la famille à se
                                         déplacer sur la route

## Partie II

### Descriptif de la demande : évolutif ?

### Objectif du client

#### À court terme

Répondre aux attentes du marché

#### À long terme

Donner une image de la société WMB d’entreprise accessible.

### Fonctions

#### Old Désignation des fonctions, la majorité peut aller dans confort

| F    | désignation                                                                                                                                 |
|:----:|:-------------------------------------------------------------------------------------------------------------------------------------------:|
| FP   | la voiture familiale **permet à** la famille de se déplacer sur la route                                                                    |
| FC1  | la voiture doit fournir un espace suffisant pour chaque utilisateur                                                                         |
| FC2  | la voiture doit maintenir les utilisateurs à une température confortable                                                                    |
| FC3  | la voiture doit permettre d’écouter la radio (et éventuellement d’autres sources audio)                                                     |
| FC4  | la voiture doit autant que possible éviter des blessures aux utilisateurs en cas d’accident                                                 |
| FC5  | la voiture doit être esthétique                                                                                                             |
| FC6  | la voiture doit disposer d’un coffre suffisamment spacieux pour contenir les affaires d’une famille à 2 ou 3 enfants                        |
| FC7  | la voiture doit alerter le conducteur lorsqu’un passager n’a pas attaché sa ceinture                                                        |
| FC8  | la voiture doit alerter le conducteur lorsqu’une porte est ouverte                                                                          |
| FC9  | la voiture doit disposer d’un mode "sécurité enfant" qui empêche l’ouverture des portes depuis l’intérieur                                  |
| FC10 | la voiture doit permettre d’installer 1 à 3 sièges enfants sur les places arrières                                                          |
| FC11 | la voiture doit disposer de 2 prises usb à l’avant (ou a défaut d’un allume cigare)                                                         |
| FC12 | la voiture doit disposer de 2 ou 3 prises usb à l’arrière en plus de ceux à l’avant                                                         |
| FC13 | la voiture doit disposer de tablettes rabattables sur les sièges arrières (permettant de poser livre, ordinateur …)                         |
| FC14 | la voiture doit posséder une autonomie suffisante pour partir en vacances                                                                   |
| FC15 | la voiture doit disposer de sièges confortables                                                                                             |
| FC16 | la voiture doit être éligible à la vignette Crit’Air 2 (si possible 1)                                                                      |
| FC17 | la voiture doit être accessible financièrement à une famille modeste, éventuellement via une offre de paiement en plusieurs fois sans frais |
| FC18 | la voiture doit disposer de systèmes d’aide à la conduite répandus actuellement                                                             |
| FC19 | la voiture doit disposer d’un emplacement de roue de secours                                                                                |
| FC20 | la voiture doit être verrouillable/dé-verrouillable à distance                                                                              |
| FC21 | la voiture doit permettre de verrouiller/déverrouiller toutes les portes en même temps                                                      |
| FC22 | la voiture doit disposer d’un système de réglage automatique des rétroviseurs                                                               |
| FC23 | la voiture doit disposer d’un régulateur de vitesse                                                                                         |

#### Désignation des fonctions

| F    | désignation                                                              |
| ---- | ------------------------------------------------------------------------ |
| FP   | la voiture familiale **permet à** la famille de se déplacer sur la route |
| FC1  | la voiture doit fournir un espace suffisant aux utilisateurs             |
| FC2  | la voiture doit être confortable                                         |
| FC3  | la voiture doit être sécurisée                                           |
| FC4  | la voiture doit être esthétique                                          |
| FC5  | la voiture doit disposer d’un coffre spacieux                            |
| FC6  | la voiture doit être adaptée à l’accueil d’enfants à l’arrière           |
| FC7  | la voiture doit être suffisamment autonome                               |
| FC8  | la voiture doit être écologique                                          |
| FC9  | la voiture doit être économe à l’utilisation                             |
| FC10 | la voiture doit être assez peu coûteuse à produire                       |
| FC11 | la voiture doit disposer de systèmes d’aide à la conduite                |
| FC12 | la voiture doit être verrouillable/dé-verrouillable à distance           |

#### Critères d’appréciations

| Fct  | Critère d’appréciation          | Niveau                                                                                                                      | Flexi |
|:----:|:-------------------------------:|:---------------------------------------------------------------------------------------------------------------------------:|:-----:|
| FP   | Homologation<br/>Puissance<br/> | Autorisée à rouler dans les zones de commercialisation<br/>Suffisante pour monter une côte avec 5 personnes et coffre plein |       |
| FC1  |                                 |                                                                                                                             |       |
| FC2  |                                 |                                                                                                                             |       |
| FC3  |                                 |                                                                                                                             |       |
| FC4  |                                 |                                                                                                                             |       |
| FC5  |                                 |                                                                                                                             |       |
| FC6  |                                 |                                                                                                                             |       |
| FC7  |                                 |                                                                                                                             |       |
| FC8  |                                 |                                                                                                                             |       |
| FC9  |                                 |                                                                                                                             |       |
| FC10 |                                 |                                                                                                                             |       |
| FC11 |                                 |                                                                                                                             |       |
| FC12 |                                 |                                                                                                                             |       |
| FC13 |                                 |                                                                                                                             |       |
| FC14 |                                 |                                                                                                                             |       |
| FC15 |                                 |                                                                                                                             |       |
| FC16 |                                 |                                                                                                                             |       |
| FC17 |                                 |                                                                                                                             |       |
| FC18 |                                 |                                                                                                                             |       |
| FC19 |                                 |                                                                                                                             |       |
| FC20 |                                 |                                                                                                                             |       |
| FC21 |                                 |                                                                                                                             |       |
| FC22 |                                 |                                                                                                                             |       |
| FC23 |                                 |                                                                                                                             |       |
