# Communication — Prise de notes

Philip K. Dick participe à une conférence sur la science fiction.
K. Dick mêle fiction, science et philosophie avec brio pour illustrer notre présent et notre futur.
Selon lui, la science fiction est au niveau de la littérature classique, mais elle à longtemps été campée au papier de mauvaise qualité, dans des magazines aux couvertures scabreuses, d’où les « Pulp Fictions ».
K. Dick place la philosophie et la psychologie partout, dans tout objet. C’est un thème récurrent dans ses œuvres.
Il avait la particularité de décrire la vie des gens ordinaires, et ses textes avaient tendance à remettre en question nos « connaissances », ce qu’est la « réalité »
Philip K. Dick était malade mentalement, ajouté à cela qu’il prenait beaucoup d’amphétamines pour écrire toute la nuit.
Il était très casanier, pour ne pas dire agoraphobe. Il prend la réalité pour non acquise, il a l’impression de ne vivre qu’à moité réellement.


