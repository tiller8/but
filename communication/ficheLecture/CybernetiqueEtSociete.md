24/09/2021
# Fiche de lecture
## Meta
Norbert Wiener, 1950, Chapitre premier - La cybernétique à travers l’histoire
Cybernétique et société

## Bio
Norbert Wiener est un enfant prodige, il sut lire à un an et demi et obtint son diplome d’études secondaires à 12 ans en 1906.
Il obtint son doctorat à Harvard à l’âge de 18 ans. Il est considéré comme le fondateur de la cybernétique.

## Keywords
Norbert Wiener, Cybernétique, Fondements, Second ouvrage
