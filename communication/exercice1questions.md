# Exercice 1 questions aux clients
## 1 l’ASCG
### 1
Quelles sont les différentes activités que vous proposez ainsi que leurs caractéristiques ?

### 2
Quelles informations avez-vous sur vos adhérents ?

### 3
Quelles sont les différentes sections de l’association ?

### 4
Quel est le processus suivi lors d’un ajout d’adhérent ?

### 5
Quelles informations avez-vous sur les animateurs ?

### 6
Pourquoi informatisez vous votre système ?

### 7
Êtes-vous déjà équipés en matériel informatique ?

### 8
Quelles sont vos attentes par rapport au projet ?

## CAO
### 1
Pourquoi informatisez vous votre système ?

### 2
Quelles informations avez-vous sur vos clients ? Particuliers/Entreprises ?

### 3
Quelles informations avez-vous sur les véhicules que vous gérez  ?

### 4
Pouvez vous nous fournir un exemple type de contrat ?

### 5
Quels acteurs agissent sur le contrat au sein de l’entreprise ?

### 6
Combien d’agences composent votre chaine ?

### 7
Êtes-vous déjà équipés en matériel informatique ?

### 8
Quelles sont vos attentes par rapport au projet ?
 
## EGB
### 1
Pourquoi informatisez vous votre système ?

### 2
Quelles informations avez-vous sur vos clients ? Particuliers/Entreprises ?

### 3
Quelles sont les différentes natures de travaux, ainsi que leur caractéristiques ?

### 4
Quelles informations avez-vous sur vos salariés ?

### 5
Quelles informations avez-vous sur vos chantiers ?

### 6
Quelles sont les différentes sections, leurs rôles et leurs échanges au sein de l’entreprise?

### 7
Êtes-vous déjà équipés en matériel informatique ?

### 8
Quelles sont vos attentes par rapport au projet ?

## S&T
### 1
Quel est votre fonctionnement interne ?
### 2
Quelles informations transitent au sein de votre entreprise ?
