# TD3 Représentation des entiers
## 1 Limites des entiers naturels
### a
$B^n-1$
### b
$B^n$
### c
∀ B, le plus grand nombre à un chiffre vaut B - 1, car il existe B nombres à un chiffres, dont 0.
Donc, le plus grand nombre représentable sur n positions est égal à :
$B-1_{n-1}B-1_{n-2}…B-1_{1}B-1_{0}$
## 2 limites des entiers relatifs
### a
$B^{n+1}$
### b
0, $B^n-0,1$
## 3
### 3.1
#### a
01001011, 00001111, 10000100
#### b
0, $B^n-1$
### 3.2
#### a
0015, 0132, 1024
#### b
0000 0000 0001 0101, 0000 0001 0011 0010, 0001 0000 0010 0100
#### c
0, $10^{n÷4}$
#### d
il faut n×4 positions binaires pour coder un nombre représenté sur n positions décimales
## 4
### 4.1
#### a
00001100, 10001100, 01111111, 11111111
#### b
le nombre +132 n’est pas représentable
### 4.2
## 5
### 5.1
### 5.2
