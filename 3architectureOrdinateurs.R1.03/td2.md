# td2
## bases numération
### a
33_9_ = 3×9+3 = 30 soit 30 fils sur terre
50_9_ = 5×9 = 45 soit 45 filles sur terre
Le vénusien à vraisemblablement 9 cornes
### b 
35+52, cette opération est définie dans toutes bases > 7.

## changement de bases
### a
98 = 49×2+0
49 = 24×2+1
24 = 12×2+0
12 = 6 ×2+0
6  = 3 ×2+0
3  = 1 ×2+1
1  = 0 ×2+1
### b
101110
0 = 0 +1 ×0
2 = 0 +2 ×1
6 = 2 +4 ×1
14= 6 +8 ×1
14= 14+16×0
46= 14+32×1

## méthodes particulières ou une base est puissance entière de l’autre

