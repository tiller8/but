# TD1 Codage de l’information

## 1

Les mots sont en format fixe avec $n=7$ lettres.

## 2

### a

- ABC

- ABA

- ACB

- ACA

- BAC

- BCA

- BAB

- BCB

- CBA

- CAB

- *CAC*

- CBC

### b

L a une puissance lexicographique de 12.

## 3

x = (2*Y/4)

10000111 01101001 11010011 01101001 10000010 00100011 10100010 10010101 11110010 01000011 10010010

## 4

01001100 01000101 00100000 01000010 01000001 01000011

LE BAC

## 5

ça÷2

11000011 10100111 01100001 11000011 10110111 00110010
