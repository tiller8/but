with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Text_IO;         use Ada.Text_IO;

procedure main is
    MAX_LENGTH : constant Integer := 15;
    type TabEntiers is array (1 .. MAX_LENGTH) of Integer;

    -- Definition du type Tableau_T
    type Tableau_T is record
        tab        : TabEntiers;
        nbElements : Integer;
    end record;
    -------------------------------------------------------------------
    -- Sous-programme permettant d'afficher le tableau tab contenu dans l'enregistrement
    -- t de type Tableau_t
    -------------------------------------------------------------------
    procedure afficherTableau (t : in Tableau_T) is
        i : Integer;
    begin
        i := 1;
        while i <= t.nbElements loop
            Put (t.tab (i));
            i := i + 1;
        end loop;
        New_Line;
    end afficherTableau;

    enrT : Tableau_T;

    function maximum (t : in Tableau_T) return Integer is
        maxi : Integer := 0;
        i    : Integer := 1;
    begin
        while i < t.nbElements loop
            if maxi < t.tab (i) then
                maxi := t.tab (i);
            end if;
            i := i + 1;
        end loop;
        return maxi;
    end maximum;

    function compterOccurence
       (element : in Integer; t : in Tableau_T) return Integer
    is
        i     : Integer := 1;
        occur : Integer := 0;
    begin
        while i < t.nbElements loop
            if t.tab (i) = element then
                occur := occur + 1;
            end if;
            i := i + 1;
        end loop;
        return occur;
    end compterOccurence;

    function elementPlusFrequent (t : in Tableau_T) return Integer is
        i    : Integer := 1;
        maxi : Integer := 0;
        elmt : Integer;
    begin
        while i < t.nbElements loop
            if compterOccurence (t.tab (i), t) > maxi then
                elmt := t.tab (i);
                maxi := compterOccurence (elmt, t);
            end if;
            i := i + 1;
        end loop;
        return elmt;
    end elementPlusFrequent;
begin
    enrT.tab        := (5, 5, 4, 45, 64, 9, 2, 2, 3, 5, 7, 8, 20, 30, 5);
    enrT.nbElements := 15;

    afficherTableau (t => enrT);

    -- tester maximum
    if elementPlusFrequent (enrT) = 5 then
        Put_Line ("function OK");
    else
        Put_Line ("function KO");
    end if;
end main;
