with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Text_IO;         use Ada.Text_IO;
with ada.Float_Text_IO;   use Ada.Float_Text_IO;

procedure notes is

   MAX_LENGTH : constant Integer := 9;
   type TabReels is array (1 .. MAX_LENGTH) of Float;

   tab:TabReels; --tableau de nbElements
   i:Integer:=1; --indice de parcours du tableau
   note:Float;   --valeur de une note entree par l'utilisateur

begin

  put("Entrez les notes, une par ligne"); New_Line;
  while i <= MAX_LENGTH loop
      get(note);
      tab(i):=note;
      i := i + 1;
  end loop;

  i := 1;
  while i <= MAX_LENGTH loop
      put(tab(i));
      i:=i+1;
  end loop;
end notes;
