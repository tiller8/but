with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Text_IO;         use Ada.Text_IO;
with ada.Float_Text_IO;   use Ada.Float_Text_IO;

procedure tableaux is

   MAX_LENGTH : constant Integer := 5;
   type TabEntiers is array (1 .. MAX_LENGTH) of Integer;

   tab1:TabEntiers; --tableau de MAX_LENGTH
   tab2:TabEntiers; --tableau de MAX_LENGTH
   tab3:TabEntiers; --tableau de MAX_LENGTH
   i:Integer:=1;    --indice de parcours du tableau

begin
  tab1:=(5,5,4,45,64);
  tab2:=(1,2,4,15,60);

  i := 1;
  while i <= MAX_LENGTH loop
      put(tab(i));
      i:=i+1;
  end loop;
end tableaux;
