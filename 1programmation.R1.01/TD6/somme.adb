with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Text_IO;         use Ada.Text_IO;

procedure somme is

   MAX_LENGTH : constant Integer := 15;
   type TabEntiers is array (1 .. MAX_LENGTH) of Integer;

   tab : TabEntiers; -- tableau de nbElements
   nbElements : Integer; -- taille r�elle du tableau tab
   i: Integer := 1; --indice de parcours du tableau
   somme : Integer := 0; -- stocke la somme des valeurs de tous les �l�ments

begin

  tab := (5,5,4,45,64,9,2,2,3,5,7,8,20,30,5);
  nbElements:=15;

  while i <= MAX_LENGTH loop
      somme := somme + tab(i);
      i := i + 1;
  end loop;

  put(somme);

end somme;

