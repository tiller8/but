with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Text_IO;         use Ada.Text_IO;

procedure polynome is

   NB_COEFS : constant Integer := 100;
   type TabInteger is array (1 .. NB_COEFS) of Integer;
   
   type Polynome is record
		coefficients: TabInteger;
		degree: integer;
	end record;
	
	procedure afficherPolynome(p: in Polynome) is
		
		index:integer:=1;
		
	begin
		
		while p.degree-index>=0 loop
			if p.coefficients(index)/=0 then
				put(p.coefficients(index));
				put("x^");
				put(p.degree-index);
				if p.degree-index/=0 then
					put(" + ");
				end if;
			end if;
			index:=index+1;
		end loop;
	end afficherPolynome;

	pol:Polynome;
	marqueurFin: Integer;
	v:integer; -- pour recuper la valeur
	i: integer:=1; -- permet le parcour des indice
   

begin

put_Line("Quel nombre voulez vous comme marqueur de fin ?"); -- recupere le marqueurFin
get(marqueurFin);new_line;

put_Line("Quel est voutre premier valeur ?"); -- recuper la premiere valeur
get(v);new_line;

while v/=marqueurFin loop
	pol.coefficients(i):= v;
	i:=i+1;
	put_Line("Quel valeur voulez vous ensuite ?"); -- recuper les valeur suivente
	get(v);new_line;
end loop;
pol.degree:=i-1;

afficherPolynome(pol);
	
end polynome;