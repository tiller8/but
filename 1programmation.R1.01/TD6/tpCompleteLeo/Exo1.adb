
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Text_IO;         use Ada.Text_IO;

procedure somme is

   MAX_LENGTH : constant Integer := 5;
   type TabEntiers is array (1 .. MAX_LENGTH) of Integer;

   tab1 : TabEntiers; -- tableau de nbElements
   tab2 : TabEntiers; -- tableau de nbElements
   tab3 : TabEntiers; -- tableau de nbElements
   nbElements : Integer; -- taille r�elle du tableau tab
   i: Integer:=1; --indice de parcours du tableau
   somme : Integer; -- stocke la somme des valeurs de tous les �l�ments

begin

	while i<=MAX_LENGTH loop
		get(somme); -- ici somme sert juste a transmetre l'info
		tab1(i):=somme;
		i:=i+1;
	end loop;
	
	i:=1;
	while i<=MAX_LENGTH loop
		get(somme); -- ici somme sert juste a transmetre l'info
		tab2(i):=somme;
		i:=i+1;
	end loop;
	
	nbElements:=5;
  
	somme:=0;
	i:=1;
--	while i<=nbElements loop
--		somme:=somme+tab(i);
--		i:=i+1;
--	end loop;
--	put(somme);

	while i<=nbElements loop
		tab3(i):=tab1(i)-tab2(i);
		put(tab3(i));
		i:=i+1;
		
	end loop;
	
end somme;

