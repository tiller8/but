with Ada.Text_IO;         use Ada.Text_IO;

procedure letters2word is

   TAILLE_MAX : constant Integer := 40;
   type TabCharacters is array (1 .. TAILLE_MAX) of character;

	tabCar: TabCharacters;
	i:integer:=1; -- pour le parcour d'indice

begin

	tabCar:=('p','r','i','n','c','e',others => ' ');
	
	while i<= TAILLE_MAX loop
		put(tabCar(i));
		i:=i+1;	
	end loop;
	
	
end letters2word;