with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

procedure test is

   TAILLE_MAX : constant Integer := 40;
   type TabCharacters is array (1 .. TAILLE_MAX) of character;
   
   MAX_LENGTH : constant Integer := 40;
   type TabEntiers is array (1 .. MAX_LENGTH) of Integer;
   
   procedure somme is

		tab1 : TabEntiers; -- tableau de nbElements
		tab2 : TabEntiers; -- tableau de nbElements
		tab3 : TabEntiers; -- tableau de nbElements
		nbElements : Integer; -- taille réelle du tableau tab
		i: Integer:=1; --indice de parcours du tableau
		somme : Integer; -- stocke la somme des valeurs de tous les éléments

	begin

		while i<=MAX_LENGTH loop
			get(somme); -- ici somme sert juste a transmetre l'info
			tab1(i):=somme;
			i:=i+1;
		end loop;
	
		i:=1;
		while i<=MAX_LENGTH loop
			get(somme); -- ici somme sert juste a transmetre l'info
			tab2(i):=somme;
			i:=i+1;
		end loop;
	
		nbElements:=5;
  
		somme:=0;
		i:=1;

		while i<=nbElements loop
			tab3(i):=tab1(i)-tab2(i);
			put(tab3(i));
			i:=i+1;
		
		end loop;
	
	end somme;


   
   procedure vingt_deux is
		tabCar: TabCharacters;
		tabEnt: tabEntiers;
		i:integer:=1; -- pour le parcour d'indice
		j:integer:=1;

	begin
		if TAILLE_MAX/=MAX_LENGTH then
			put("Error");
		else
	
			tabCar:=('a','c','f',others => ' ');
			tabEnt:=(1,2,4, others => 0);
	
			while i<= TAILLE_MAX loop
				j:=0;
				while j<tabEnt(i) loop
					put(tabCar(i));
					j:=j+1;
				end loop;
				i:=i+1;	
			end loop;
		end if;
	end vingt_deux;
		
	procedure deux is 
		
		tabCar: TabCharacters;
		i:integer:=1; -- pour le parcour d'indice

	begin

		tabCar:=('p','r','i','n','c','e',others => ' ');
	
		while i<= TAILLE_MAX loop
			put(tabCar(i));
			i:=i+1;	
		end loop;
	
	
	end deux;

begin
	
	deux;
	vingt_deux;
	somme;
	
end test;