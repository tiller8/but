with Traceur; use Traceur;

procedure tracerdessin  is
    motifs:integer;   -- nombre de motifs d�j� trac�s
    branches:integer; -- nombre de branches du motif en cours d�j� trac�es
    c1:integer;       -- compteur d'unit�s de distance
begin
	-- initialise le traceur
	initialiserTraceur;
	
	-- � completer 
	-- ne trace pas, centre le traceur, oriente vers le nord
    leverStylet;
    centrerStylet;
    orienterNord;

    -- place le traceur en position du premier motif
    c1 := 0;
    while c1 < 200 loop
        deplacerStylet;
        pivoterDroite;
        deplacerStylet;
        pivoterGauche;
        c1 := c1 + 1;
    end loop;

    -- oriente le traceur vers la droite
    pivoterGauche;

    -- trace les 5 motifs
    motifs := 0;
    while motifs < 5 loop
        pivoterGauche;
        -- trace un motif
        branches := 0;
        while branches < 3 loop
            -- trace une branche du motif
            baisserStylet;
            c1 := 0;
            while c1 < 15 loop
                deplacerStylet;
                c1 := c1 + 1;
            end loop;
            -- mise en position du traceur pour la branche suivante
            leverStylet;
            c1 := 0;
            while c1 < 15 loop
                deplacerStylet;
                c1 := c1 + 1;
            end loop;
            pivoterGauche;
            branches := branches + 1;
        end loop;
        motifs := motifs + 1;
        -- mise en position pour le motif suivant
        c1 := 0;
        while c1 < 30 loop
            deplacerStylet;
            c1 := c1 + 1;
        end loop;
        c1 := 0;
        while c1 < 50 loop
            deplacerStylet;
            pivoterGauche;
            deplacerStylet;
            pivoterDroite;
            c1 := c1 + 1;
        end loop;
    end loop;
   
    -- affiche le dessin
	afficherTraceur;

end tracerdessin;
