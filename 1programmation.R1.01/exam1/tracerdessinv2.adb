with Traceur; use Traceur;

procedure tracerdessinv2  is
    motifs:integer;   -- nombre de motifs d�j� trac�s
    branches:integer; -- nombre de branches du motif en cours d�j� trac�es
    lignes:integer;   -- nombre de lignes d�j� trac�es
    c1:integer;       -- compteur d'unit�s de distance
begin
	-- initialise le traceur
	initialiserTraceur;
	
	-- � completer 
	-- ne trace pas, centre le traceur, oriente vers le nord
    leverStylet;
    centrerStylet;
    orienterNord;

    -- place le traceur en position du premier motif
    c1 := 0;
    while c1 < 200 loop
        deplacerStylet;
        pivoterDroite;
        deplacerStylet;
        pivoterGauche;
        c1 := c1 + 1;
    end loop;

    -- oriente le traceur vers la gauche
    pivoterGauche;
    pivoterGauche;

    -- trace les 5 lignes
    lignes := 0;
    while lignes < 5 loop
        -- trace les motifs
        motifs := 0;
        while motifs < 5 - lignes loop
            -- trace un motif
            branches := 0;
            while branches < 3 loop
                -- trace une branche du motif
                baisserStylet;
                c1 := 0;
                while c1 < 15 loop
                    deplacerStylet;
                    c1 := c1 + 1;
                end loop;
                -- mise en position du traceur pour la branche suivante
                leverStylet;
                c1 := 0;
                while c1 < 15 loop
                    deplacerStylet;
                    c1 := c1 + 1;
                end loop;
                pivoterGauche;
                branches := branches + 1;
            end loop;
            motifs := motifs + 1;
            -- mise en position pour le motif suivant
            c1 := 0;
            while c1 < 80 loop
                deplacerStylet;
                c1 := c1 + 1;
            end loop;
            pivoterGauche;
        end loop;
        lignes := lignes + 1;
        c1 := 0;
        while c1 < 50 loop
            deplacerStylet;
            c1 := c1 + 1;
        end loop;
        pivoterGauche;
        c1 := 0;
        while c1 < 50 * (5 - lignes) loop
            deplacerStylet;
            c1 := c1 + 1;
        end loop;
        pivoterDroite;
    end loop;
   
    -- affiche le dessin
	afficherTraceur;

end tracerdessinv2;
