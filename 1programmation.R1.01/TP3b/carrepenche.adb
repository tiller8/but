with traceur; use traceur;

procedure carrepenche is
   cotes : Integer; -- cot�s du carr� d�j� trac�s
   traits : Integer; -- unit�s du trait d�j� trac�es
begin
	-- initialise le traceur
	initialiserTraceur;

    -- centre et oriente le traceur
    leverStylet;
    centrerStylet;
    orienterNord;

    -- tracer les 4 cot�s
    baisserStylet;
	cotes:=0;
    while cotes < 4 loop
        traits := 0;
		while traits < 50 loop
			deplacerStylet;
            pivoterDroite;
            deplacerStylet;
            pivoterGauche;
			traits:= traits + 1;
		end loop;
        pivoterGauche;
        cotes := cotes + 1;
    end loop;

    -- affiche le dessin
	afficherTraceur;
end carrepenche;
