with traceur; use traceur;

procedure carrescoins is
   divisions : Integer; -- divisions de la figure d�j� trac�es
   cotes : Integer; -- cot�s de 50 unit�s de la division d�j� trac�s
   trait : Integer; -- unit�s du trait d�j� trac�es
   distance : Integer; -- nb unit�s de distance entre le traceur et le centre (distance de manhattan)
begin
	    -- initialise le traceur
		initialiserTraceur;

	    -- ne pas tracer, centrer le stylet, orienter le stylet vers nord
		leverStylet;
		centrerStylet;
		orienterNord;

        -- placer stylet en position de d�part
        distance := 0;
        while distance <= 50 loop
            deplacerStylet;
            distance := distance + 1;
        end loop;
        pivoterGauche;
        distance := 0;
        while distance <= 100 loop
            deplacerStylet;
            distance := distance + 1;
        end loop;
        pivoterGauche;

	    -- tracer les carr�s
        divisions:=0;
        while divisions < 4 loop
            -- orienter dans la nouvelle direction
            pivoterGauche;

            -- tracer les 3 cot�s de la figure r�p�t�e
            baisserStylet;
            trait := 0;
		    while trait < 200 loop
			    deplacerStylet;
			    trait:= trait + 1;
            end loop;
		    cotes:=0;
            while cotes < 2 loop
                pivoterGauche;
                trait := 0;
		        while trait < 50 loop
			        deplacerStylet;
			        trait:= trait + 1;
		        end loop;
                cotes := cotes + 1;
            end loop;
            divisions := divisions + 1;
        end loop;

        -- affiche le dessin
		afficherTraceur;
end carrescoins;
