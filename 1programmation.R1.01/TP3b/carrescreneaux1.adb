with traceur; use traceur;

procedure carrescreneaux is
    cotes : Integer ; -- c�t�s d�j� trac�s
    creneaux : Integer; -- parties �l�mentaires de cr�neau d�j� trac�es
    traits : Integer; -- unit�s de trait d�j� trac�es
begin
	-- initialise le traceur
	initialiserTraceur;

	-- ne pas tracer, centrer le stylet, orienter le stylet vers nord
	leverStylet;
	centrerStylet;
	orienterNord;

	-- tracer les carr�s
    baisserStylet;
    cotes:=0;
    while cotes < 2 loop
        -- trace le premier trait du cot�
        traits := 0;
		while traits < 20 loop
		    deplacerStylet;
			traits:= traits + 1;
		end loop;
        pivoterDroite;

        -- tracer les 12 bouts de cr�neaux
        creneaux := 0;
        while creneaux < 12 loop
            traits := 0;
		    while traits < 20 loop
			    deplacerStylet;
			    traits:= traits + 1;
		    end loop;
            if (creneaux / 2) mod 2 = 0 or creneaux = 11 then
                pivoterGauche;
            else
                pivoterDroite;
            end if;
            creneaux := creneaux + 1;
        end loop;
        cotes := cotes + 1;
    end loop;

    -- affiche le dessin
	afficherTraceur;
end carrescreneaux;
