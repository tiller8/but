with traceur; use traceur;

procedure carres is
   carres : Integer; -- carr�s d�j� trac�s
   cotes : Integer; -- cot�s du carr� d�j� trac�s
   traits : Integer; -- unit�s du trait d�j� trac�es
   distance : Integer; -- nb unit�s de distance entre le traceur et le centre
begin
	    -- initialise le traceur
		initialiserTraceur;

	    -- tracer les carr�s
        carres:=0;
        while carres < 5 loop
	        -- ne pas tracer, centrer le stylet, orienter le stylet vers nord
		    leverStylet;
		    centrerStylet;
		    orienterNord;

            -- placer stylet en haut � droite du carr� � tracer
            distance := 0;
            while distance <= (10 + carres * 25) loop
                deplacerStylet;
                pivoterGauche;
                deplacerStylet;
                pivoterDroite;
                distance := distance + 1;
            end loop;

            -- orienter dans la nouvelle direction
            pivoterDroite;

            -- tracer les 4 cot�s
		    cotes:=0;
            baisserStylet;
            while cotes < 4 loop
                traits := 0;
		        while traits < 20 + carres * 50 loop
			        deplacerStylet;
			        traits:= traits + 1;
		        end loop;
                pivoterDroite;
                cotes := cotes + 1;
            end loop;
            carres := carres + 1;
        end loop;

        -- affiche le dessin
		afficherTraceur;
end carres;
