with traceur; use traceur;

procedure carrescreneaux2 is
    creneaux : Integer; -- parties �l�mentaires de cr�neau d�j� trac�es
    traits : Integer; -- unit�s de trait d�j� trac�es
begin
	-- initialise le traceur
	initialiserTraceur;

	-- ne pas tracer, centrer le stylet, orienter le stylet vers nord
	leverStylet;
	centrerStylet;
	orienterNord;

	-- tracer les carr�s
    baisserStylet;
    -- tracer les 48 bouts de cr�neaux
    creneaux := 0;
    while creneaux < 12 loop
        -- trace le premier trait du cot�
        if creneaux mod 12 = 0 then
            traits := 0;
		    while traits < 20 loop
		        deplacerStylet;
			    traits:= traits + 1;
	    	end loop;
            pivoterDroite;
        end if;

        traits := 0;
		while traits < 20 loop
			deplacerStylet;
			traits:= traits + 1;
		end loop;
        if (creneaux / 2) mod 2 = 0 or creneaux / 11 = 0 then
            pivoterGauche;
        else
            pivoterDroite;
        end if;
        creneaux := creneaux + 1;
    end loop;

    -- affiche le dessin
	afficherTraceur;
end carrescreneaux2;
