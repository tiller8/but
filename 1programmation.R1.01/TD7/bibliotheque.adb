with text_io; use text_io;
with ada.integer_text_io; use ada.integer_integer_text_io;

type TabCharacter is array (1 .. max_length) of integer;

type message is record
    text: TabCharacter;
    nbCharacters: integer;
    cle: integer;
end record;

procedure liremessage (m: out message) is 
    continue: char:= "y";
    while continue = "y" or continue = "Y" loop
        get(m.text(m.nbCharacters + 1))
        m.nbCharacters:= m.nbCharacters + 1;
        put("y pour continuer a enregistrer"); new_line;
        get(continue)
    end loop;

max_length: integer:= 1000;
