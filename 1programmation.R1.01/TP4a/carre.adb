with Text_IO; use Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

procedure carre is
    -- trace une ligne de n caract�res character
    procedure printline (n: in integer; car: in character) is
        nb:integer:=0;  -- nombre de caract�res trac�s
    begin
        while nb < N loop
            put(car);
            nb := nb+1;
        end loop;
    end printline;

    -- trace un carre de c caract�res de c�t�
    procedure printcarre (c: in integer) is
        c_in:integer:=c;  -- c en variable interne
        total:integer:=c; -- nombre total de caract�res du c�t�
    begin
        -- c : nombre de lignes � tracer
        while c_in > 0 loop
            if c_in = total or c_in = 1 then
                printline(total,'*');
            else
                put('*');
                printline(total - 2,' ');
                put('*');
            end if;
            new_line;
            c_in := c_in - 1;
        end loop;
    end printcarre;

    cote:integer; -- nombre caract�res c�t� carr�
begin
    put("Quelle taille pour le carr� ? : "); get(cote);
    new_line;
    printcarre(cote);
end carre;
