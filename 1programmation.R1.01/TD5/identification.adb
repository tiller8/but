with text_io; use text_io;
with ada.integer_text_io; use ada.integer_text_io;

procedure identification is
    code:integer;               -- code entré par l'utilisateur
    somme:integer;              -- somme des  3 premiers chiffres du code
    incorrect:boolean:=true;    -- vrai si code incorrect
begin
    while incorrect loop
        put("Entrez un code d’identification de quatre chiffres : ");
        get(code);
        somme := (code/1000)+(code/100) mod 10+(code/10) mod 10;
        if code mod 10 = somme mod 7 then
            put("Ce code est correct. Prêt à photocopier.");
            incorrect:=false;
        else
            put("Ce code est incorrect pour ce photocopieur.");
            new_line; new_line;
        end if;
    end loop;

end identification;
