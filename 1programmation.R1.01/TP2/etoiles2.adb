with ada.text_io;use ada.text_io;
with ada.integer_text_io;use ada.integer_text_io;

procedure etoiles2 is
    height:integer; --nombre total de lignes à tracer
    lines:integer:=0; --nombre de lignes déjà tracées
    characters:integer:=0; --nb de caractères " " puis "*" tracés dans la ligne
begin
    put("Entrez la hauteur de la figure : ");
    get(height);

    while lines < height loop
        characters := 0;
        while characters < height - lines - 1 loop
            put(" ");
            characters := characters + 1;
        end loop;
        characters := 0;
        while characters <= lines loop
            put("* ");
            characters := characters + 1;
        end loop;
        lines := lines + 1;
        new_line;
    end loop;

end etoiles2;
