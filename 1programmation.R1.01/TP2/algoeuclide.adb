with ada.text_io;use ada.text_io;
with ada.integer_text_io;use ada.integer_text_io;

procedure algoEuclide is
    a:integer;
    b:integer;
    r:integer;
begin
    put("Entrez le premier nombre : ");
    get(a); 
    put("Entrez le second nombre : ");
    get(b);
    r := a mod b;
    while r /= 0 loop
        a := b;
        b := r;
        r := a mod b;
    end loop;
    put("Le PGCD des deux nombres est : ");
    put(b);
end algoEuclide;
