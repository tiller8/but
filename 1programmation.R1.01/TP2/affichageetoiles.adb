with ada.text_io;use ada.text_io;
with ada.Integer_text_io;use ada.Integer_text_io;

procedure affichageEtoiles is
    NbEtoiles:Integer;
begin
    put("Entrez le nombre d’etoiles : ");
    get(NbEtoiles);
    
    while NbEtoiles > 0 loop
        put(" * ");
        NbEtoiles := NbEtoiles - 1;
    end loop;

end affichageEtoiles;
