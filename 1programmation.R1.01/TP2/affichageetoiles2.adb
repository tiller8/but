with ada.text_io;use ada.text_io;
with ada.Integer_text_io;use ada.Integer_text_io;

procedure affichageEtoiles is
    NbEtoiles:integer;
    NbLignes:integer;
    i:integer;
begin
    put("Entrez le nombre d’etoiles : ");
    get(NbEtoiles);
    put("Entrez le nombre de lignes : ");
    get(NbLignes);
    while NbLignes > 0 loop
        i := NbEtoiles;
        while i > 0 loop
            put(" * ");
            i := i - 1;
        end loop;
        new_line;
        NbLignes := NbLignes - 1;
        NbEtoiles := NbEtoiles - 1;
    end loop;

end affichageEtoiles;
