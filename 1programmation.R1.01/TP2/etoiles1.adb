with ada.text_io;use ada.text_io;
with ada.Integer_text_io;use ada.Integer_text_io;

procedure etoiles1 is
    hauteur:integer;
    c1:integer:=0;
    c2:integer;
begin
    put("Entrez la hauteur de la figure : ");
    get(hauteur);
    while c1 < hauteur loop
        c2 := 0;
        c1 := c1 + 1;
        while c2 < c1 loop
            put("* ");
            c2 := c2 + 1;
        end loop;
        new_line;
    end loop;

end etoiles1;
