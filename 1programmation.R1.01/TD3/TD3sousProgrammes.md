# Réponses TD3
## Exercice 1
### a
Le corps du programme principal commande par "procedure", se nomme "testSomme" et se termine par "end testSomme;".
Le coprs du sous-programme commence par "function", le sous-programme se nomme "somme", et son corps se termine par "end somme;"

### b
#### Glossaire programme principal :
firstValue, secondValue, thirdValue, firstResult, thirdResult, fourthResult
firstResult n’est pas accessible ne peut pas être utilisée dans le sous-programme
#### Glossaire sous-programme :
resultat
La variable resultat n’est pas accessible depuis le programme principal.

### c
Le sous programme utilisé est une fonction. Nous avons vus les sous-programmes de type fonction et procedure.
Le sous-programme utilise l’instruction "return $resultat" pour envoyer la valeur de retour.

### d
le programme principal appelle le sous-programme avec l’instruction somme(valeur un, valeur deux).
Le résultat de l’appel est récupéré dans la variable assignée au résultat de l’appel, par éxemple résultat : résultat := somme(valeur1,valeur2…)

### e
Le programme principal transmet des données au sous-programme via les paramètres effectifs, qui les assigne à des paramètres formels.

### f

### g

### h

