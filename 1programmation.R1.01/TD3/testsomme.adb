with ada.text_io; use ada.text_io;
with ada.integer_text_io; use ada.integer_text_io;
with ada.float_text_io; use ada.float_text_io;

procedure testSomme is
    function somme (x : in Integer; y : in Integer) return Integer is
        resultat : Integer;
    begin
        resultat := x+y;
        return resultat;
    end somme;

    function fracSum (n : in Integer) return Integer is
        result : float;
        c : Integer := 0;
    begin
        while c <= n
        return resultat;
    end fracSum;

    firstValue : Integer;
    secondValue : Integer;
    thirdValue : Integer;

    firstResult : Integer;
    secondResult : Integer;
    thirdResult : Integer;
    fourthResult : Integer;
begin
    put("Première valeur : ");
    get(firstValue);
    put("Seconde valeur : ");
    get(secondValue);
    put("Troisième valeur : ");
    get(secondValue);

    firstResult:=somme(x=>firstValue,y=>secondValue);
    secondResult:=somme(x=>secondValue,y=>firstValue);
    thirdResult:=somme(x=>firstValue,y=>thirdValue);
    fourthResult:=somme(x=>secondValue,y=>thirdValue);

    put(firstResult);
    put(secondResult);
    put(thirdResult);
    put(fourthResult);

end testSomme;
    
