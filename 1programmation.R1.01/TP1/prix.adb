with ada.text_io;use ada.text_io;
with ada.Integer_text_io;use ada.Integer_text_io;

procedure prix is

	nb:Integer; --nombre de calendriers, puis prix à payer

begin
	put("Entrez le nombre de calendriers : "); --demande le nb de calendriers
	get(nb); --place la réponse de l’utilisateur dans la variable nb

	if nb < 75 then
		nb := nb;
	else
		nb := (nb - 10 + 3);
	end if;

	nb := nb + 4;
	put(nb);

end prix;
