with ada.Integer_text_io;use ada.Integer_text_io;
with ada.text_io;use ada.text_io;

procedure mention is

	note:Integer; --stockage de la note dans un entier

begin
    --demande la note
	put("Entrez la note : "); 
    --place la réponse de l’utilisateur dans la variable note
	get(note); 

	if note >= 14 then
		if note >= 16 then
            if note > 20 then
                -- si note > 20
                put("Note invalide");
            else
                -- si note >= 16
			    put("Mention très bien, bravo !");
            end if;
		else
            --si note >= 14
			put("Mention bien");
		end if;
	else
		if note >= 12 then
            --si note >= 12
			put("Mention assez bien");
		else
			if note >= 10 then
                --si note >= 10
				put("Mention passable");
			else
                if note < 0 then
                    --si note < 0
                    put("Note invalide");
                else
                    --si note < 10
				    put("Mention insuffisant");
                end if;
			end if;
		end if;
	end if;

end mention;
