with ada.text_io;use ada.text_io;
with ada.Integer_text_io;use ada.Integer_text_io;

procedure algo is
	-- On initialise les variables a et i, respectivement aux valeurs 2 et 1
	a:Integer:=2;
	i:Integer:=1;
	c:Integer; -- On initialise la variable c, sans valeur

begin

	while i <= 5 loop -- Tant que i est inférieur ou égal à 5, condition d’arrêt
		c := a; -- c prend la valeur de a
		a := c + 3; -- a prend la valeur de c + 3
		i := i + 1; -- i est incrémenté de 1, progression
	end loop;

	put(a); -- affiche la valeur de a
end algo;
