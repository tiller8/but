with ada.text_io;use ada.text_io; -- Importe la bibliothèque permettant les entrées/sorties avec du texte

procedure hello is -- Commence la définition de la procédure hello

begin -- Sépare la zone d’initialisation de la zone d’exécution de la procédure

	put("Hello"); -- Affiche le message Hello dans la console
	New_Line; -- Saute une ligne
	put("Guilhem Fauré"); -- Affice mon prénom et nom dans la console

end hello; -- Met fin à la définition de la procédure hello
